# -----------------------------------------------------------------------------
# DAQ AETHALOMETER PROJECT -- PEAK IDENTIFICATION FUNCTIONS
'''
This script contains functions for counting peaks over various time ranges
'''
# -----------------------------------------------------------------------------


def countBurn(df):

    burns = [0] * 3

    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1:

            ts = int(df['DayType'].iloc[i]) - 1
            burns[ts] += 1

        i += 1

    return burns

# -----------------------------------------------------------------------------


def countDay(df):

    # initialize list of days with values of zero
    days = [0] * 7

    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1:

            ts = int(df['DATETIME'].iloc[i].weekday())
            days[ts] += 1

        i += 1

    # pass the resultant list out
    return days


# -----------------------------------------------------------------------------

def countDayTypes(df):

    # initialize lists of days per week (one list per day-type)

    daysOK = [0] * 7
    daysMOD = [0] * 7
    daysBAD = [0] * 7

    # -------------------------------------------------------------------------
    # find count of peaks occuring when day-type==OK
    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1 and int(df['DayType'].iloc[i]) == 1:

            ts = int(df['DATETIME'].iloc[i].weekday())
            daysOK[ts] += 1

        i += 1

    # -------------------------------------------------------------------------
    # find count of peaks occuring when day-type==OK
    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1 and int(df['DayType'].iloc[i]) == 2:

            ts = int(df['DATETIME'].iloc[i].weekday())
            daysMOD[ts] += 1

        i += 1

    # -------------------------------------------------------------------------
    # find count of peaks occuring when day-type==OK
    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1 and int(df['DayType'].iloc[i]) == 3:

            ts = int(df['DATETIME'].iloc[i].weekday())
            daysBAD[ts] += 1

        i += 1

    return daysOK, daysMOD, daysBAD


# -----------------------------------------------------------------------------

def countHour(df):

    hours = [0] * 24

    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1:

            ts = df['DATETIME'].iloc[i].hour
            hours[ts] += 1

        i += 1

    return hours


# -----------------------------------------------------------------------------

def countHourTypes(df):

    # initialize lists of hours per day (one list per day-type)

    hoursOK = [0] * 24
    hoursMOD = [0] * 24
    hoursBAD = [0] * 24

    # -------------------------------------------------------------------------
    # find count of peaks occuring when day-type==OK
    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1 and int(df['DayType'].iloc[i]) == 1:

            ts = df['DATETIME'].iloc[i].hour
            hoursOK[ts] += 1

        i += 1

    # -------------------------------------------------------------------------
    # find count of peaks occuring when day-type==MODERATE
    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1 and int(df['DayType'].iloc[i]) == 2:

            ts = df['DATETIME'].iloc[i].hour
            hoursMOD[ts] += 1

        i += 1

    # -------------------------------------------------------------------------
    # find count of peaks occuring when day-type==BAD
    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1 and int(df['DayType'].iloc[i]) == 3:

            ts = df['DATETIME'].iloc[i].hour
            hoursBAD[ts] += 1

        i += 1

    return hoursOK, hoursMOD, hoursBAD


# -----------------------------------------------------------------------------

def countMonth(df):

    # initialize list of months with values of zero
    months = [0] * 12

    i = 0

    while i < len(df):

        if df['Peak'].iloc[i] == 1:

            ts = df['DATETIME'].iloc[i].month - 1
            months[ts] += 1

        i += 1

    # pass the resultant list out
    return months
