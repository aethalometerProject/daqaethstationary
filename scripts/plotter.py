#------------------------------------------------------------------------------
import pandas as pd
import os
from datetime import datetime, timedelta
import numpy as np

#import matplotlib as plt
# import matplotlib.pyplot as plt
# import matplotlib.dates as mdates
# from pandas.plotting import register_matplotlib_converters
# register_matplotlib_converters()


import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


print(matplotlib.get_backend())

dayss = mdates.DayLocator()      # every day
months = mdates.MonthLocator()  # every month

cwd = os.getcwd()

#------------------------------------------------------------------------------
# PLOTTER HELPS: NEEDED FOR LATEX
path = '/Library/TeX/texbin:'
path2 = '/Library/TeX/texbin/kpsewhich:'
path3 = '/Library/TeX/texbin/latex:'
os.environ['PATH'] += path
os.environ['PATH'] += path2
os.environ['PATH'] += path3


#------------------------------------------------------------------------------
def plotter(df_y, df_yy, df_SD, dfy_col, dfyy_col, location, filename, color, linestyle, labely, labelyy, units, startDTO, endDTO):

    timeD = endDTO - startDTO
    days = int(timeD.days)
    startDTOadj = startDTO + timedelta(1)

    plt.rcParams['text.usetex'] = True
    plt.rcParams['figure.figsize'] = [14, 6]

    # subplot: build new figure & axis via subplot; second axis sharing x
    fig, ax = plt.subplots()
    ax2 = ax.twinx()

    # subplot: axis ax data is delta carbon; axis ax2 data is heat deficit

    # ax.plot(dataBV['DATETIME'],dataBV['delta'], label="Delta-C")
    ax.plot(df_y['DATE'], df_y[dfy_col], label=labely)
    # ax2.plot(heatDef['DATE'],heatDef['HEATDEF'], 'o--',color = 'C4', label="Heat Deficit")
    ax2.plot(df_yy['DATE'], df_yy[dfyy_col],
             linestyle, color=color, label=labelyy)

    # Find minimum of the y axis to plot the Day Type Bar
    ymin, ymax = ax.get_ylim()
    ymin2, ymax2 = ax2.get_ylim()

    if ymin < ymin2:
        new_ymin = ymin - 0
        ax.set_ylim(bottom=new_ymin)
        DayTypeBar_ymin = new_ymin
        min_indicator = True
    else:
        new_ymin = ymin2 - 0
        ax2.set_ylim(bottom=new_ymin)
        DayTypeBar_ymin = new_ymin
        min_indicator = False

    # Plot Day Type Bar
    for i in range(len(df_SD) - 1):
        dateline = [df_SD.index[i], df_SD.index[i + 1]]
        deltaline = [DayTypeBar_ymin, DayTypeBar_ymin]
        if min_indicator == True:
            line = ax.plot(dateline, deltaline,
                           color=df_SD.iloc[i], linewidth=12.0)
        else:
            line = ax2.plot(dateline, deltaline,
                            color=df_SD.iloc[i], linewidth=12.0)

    # subplot: set axis labels, ticks, positions for ax and ax2; format xaxis
    # green one on RHS  (toggle for RHS axes)
    ax2.spines["right"].set_position(("axes", 1))
    # ax2.spines["left"].set_position(("axes", -0.2)) # green one on LHS  (toggle for LHS axes)
    ax2.yaxis.set_label_position('right')
    ax2.yaxis.set_ticks_position('right')
    ax.set_ylabel(labely)
    ax2.set_ylabel(labelyy + " " + units + "      ")
    plt.gcf().autofmt_xdate()

    # subplot: set grid; set title; set legend; tweak layout(?)
    ax.grid(True)
    ax.set_title(location + "\n%i day(s) showing" %
                 days, loc='left', fontsize=13)
    ax.set_title("Starting: " + startDTOadj.strftime('%Y-%m-%d') + "\nEnding: " +
                 endDTO.strftime('%Y-%m-%d'), loc='right', fontsize=11, color='grey', style='italic')
    ax.legend(loc=2)
    ax2.legend(loc=1)

    import matplotlib.patches as mpatches

    red_patch = mpatches.Patch(color='red', label='Mandatory')
    orange_patch = mpatches.Patch(color='orange', label='Voluntary')
    green_patch = mpatches.Patch(color='green', label='Unrestricted')

    leg1 = ax.legend(bbox_to_anchor=(1.05, 1.025), loc=2, borderaxespad=0.)
    leg2 = ax2.legend(bbox_to_anchor=(1.05, 0.945), loc=2, borderaxespad=0.)
    leg3 = ax2.legend(handles=[red_patch, orange_patch, green_patch], bbox_to_anchor=(
        1.05, 0.865), loc=2, borderaxespad=0.)
    ax2.add_artist(leg2)

    # plt.tight_layout()
    # subplot: render plot
    # plt.show()

    # return fig

    import os

    cwd = os.getcwd()

    #final_path = '/daqFlask'
    #outfile = 'plot_' + location + '_' + labely + '-' + labelyy + '.png'

    final_path2 = cwd + filename

    #plt.savefig(outfile, bbox_inches='tight')

    plt.savefig(final_path2, bbox_inches='tight')
    plt.close()

#------------------------------------------------------------------------------


def plotterPeaksDays(df1, df2, df3, xLabel, filename):
    # plot per days of week
    plt.rcdefaults()
    fig, ax = plt.subplots()
    barWidth = 0.25

    #weekDays = ('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN')
    y_pos = np.arange(len(xLabel))
    y_pos2 = [x + barWidth for x in y_pos]
    y_pos3 = [z + barWidth for z in y_pos2]

    ax.bar(y_pos, df1, barWidth + 0,
           color='orange', alpha=0.5, label='BV')
    ax.bar(y_pos2, df2, barWidth + 0,
           color='green', alpha=0.5, label='LN')
    ax.bar(y_pos3, df3, barWidth + 0,
           color='blue', alpha=0.5, label='SM')

    ax.set(xticks=y_pos + barWidth, xticklabels=xLabel,
           xlim=[2 * barWidth - 1, len(xLabel)])
    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=3)

    ax.set_ylabel('Number of Events')
    ax.set_title('Delta Carbon Events for Monitored Areas by Day of Week\n\n')

    # plt.show()
    outPNG = os.path.expanduser(
        cwd + '/dynamic/plots/deltaCarbonEvents_' + filename + '.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()


#------------------------------------------------------------------------------

def plotterPeaksDaysType(df1, df2, df3, xLabel, filename, title):
    # plot per days of week
    plt.rcdefaults()
    fig, ax = plt.subplots()
    barWidth = 0.25

    #weekDays = ('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN')
    y_pos = np.arange(len(xLabel))
    y_pos2 = [x + barWidth for x in y_pos]
    y_pos3 = [z + barWidth for z in y_pos2]

    ax.set_ylim(top=3.1)

    ax.bar(y_pos, df1, barWidth + 0,
           color='orange', alpha=0.5, label='BV')
    ax.bar(y_pos2, df2, barWidth + 0,
           color='green', alpha=0.5, label='LN')
    ax.bar(y_pos3, df3, barWidth + 0,
           color='blue', alpha=0.5, label='SM')

    ax.set(xticks=y_pos + barWidth, xticklabels=xLabel,
           xlim=[2 * barWidth - 1, len(xLabel)])
    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=3)

    ax.set_ylabel('Number of Events')
    ax.set_title(title)

    # plt.show()
    outPNG = os.path.expanduser(
        cwd + '/dynamic/plots/deltaCarbonEvents_' + filename + '.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()

#------------------------------------------------------------------------------


def plotterPeaksHours(df1, df2, df3, xLabel, filename):

    plt.rcdefaults()
    fig, ax = plt.subplots()
    barWidth = 0.3
    #hours = ('00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00')
    y_pos = np.arange(len(xLabel))
    y_pos2 = [x + barWidth for x in y_pos]
    y_pos3 = [z + barWidth for z in y_pos2]

    ax.bar(y_pos, df1, barWidth + 0, color='orange', alpha=0.5, label='BV')
    ax.bar(y_pos2, df2, barWidth + 0, color='green', alpha=0.5, label='LN')
    ax.bar(y_pos3, df3, barWidth + 0, color='blue', alpha=0.5, label='SM')

    ax.set(xticks=y_pos + barWidth, xticklabels=xLabel,
           xlim=[2 * barWidth - 1, len(xLabel)])

    #plt.xticks(y_pos, hours)
    plt.ylabel('Number of Events')
    plt.title('Delta Carbon Events for Monitored Areas by Hour of Day\n\n')
    plt.xticks(rotation=90)
    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=3)

    # plt.show()
    outPNG = os.path.expanduser(
        cwd + '/dynamic/plots/deltaCarbonEvents_' + filename + '.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()

#------------------------------------------------------------------------------


def plotterPeaksHoursType(df1, df2, df3, xLabel, filename, title):

    plt.rcdefaults()
    fig, ax = plt.subplots()
    barWidth = 0.3
    y_pos = np.arange(len(xLabel))
    y_pos2 = [x + barWidth for x in y_pos]
    y_pos3 = [z + barWidth for z in y_pos2]

    ax.bar(y_pos, df1, barWidth + 0, color='orange', alpha=0.5, label='BV')
    ax.bar(y_pos2, df2, barWidth + 0, color='green', alpha=0.5, label='LN')
    ax.bar(y_pos3, df3, barWidth + 0, color='blue', alpha=0.5, label='SM')

    #ymin, ymax = ax.get_ylim()
    ax.set_ylim(top=5.1)

    ax.set(xticks=y_pos + barWidth, xticklabels=xLabel,
           xlim=[2 * barWidth - 1, len(xLabel)])

    #plt.xticks(y_pos, hours)
    plt.ylabel('Number of Events')
    plt.title(title)
    plt.xticks(rotation=90)
    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=3)
    # plt.show()
    outPNG = os.path.expanduser(
        cwd + '/dynamic/plots/deltaCarbonEvents_' + filename + '.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()
