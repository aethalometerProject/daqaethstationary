# LIBRARY IMPORTS
import pandas as pd
import csv
import os
from datetime import datetime, timedelta
import time
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import matplotlib.pyplot as plt
plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scripts.count import countBurn, countDay, countDayTypes, countHour, countHourTypes, countMonth
from scripts.plotter import plotterPeaksDays, plotterPeaksDaysType, plotterPeaksHours, plotterPeaksHoursType
# from scripts.plotterPeaksDays import plotterPeaksDays
# from scripts.plotterPeaksHours import plotterPeaksHours
# from scripts.plotterPeaksDaysType import plotterPeaksDaysType
# from scripts.plotterPeaksHoursType import plotterPeaksHoursType


# -----------------------------------------------------------------------------
# get the current working directory; spec the station list
cwd = os.getcwd()
stations = ['BV', 'LN', 'SM']

# -----------------------------------------------------------------------------
# this function identifies peaks based on a deltaCarbon threshold value


def generatorPeaks():

    # TASK: if we make this function customizable maybe use the 3 next lines
    # # make station identifiers uppercase for later work
    # stations = [x.upper() for x in stations]
    # print(stations)

    # this threshold value can be adjusted; is low deltaCarbon val for a peak
    threshold = 1000
    t = threshold

    # loop across the aethalometer data (all collected data is considered)
    for i in range(len(stations)):

        dataFile = os.path.expanduser(
            cwd + '/static/data/aeth-burn-days/burn' + stations[i] + '.csv')

        df = pd.read_csv(dataFile, index_col=0, header=0)

        # initialize the peak identification column with zeros
        df['Peak'] = int(0)

        j = 0
        k = 1

        # this loop identifies peaks based on the given threshold
        while j < len(df):

            k = 1

            if df['delta'].iloc[j] > t:

                df['Peak'].iloc[j] = 1

                while df['delta'].iloc[j + k] > t:

                    k += 1

            j += k

        outPath = os.path.expanduser(
            cwd + '/dynamic/data/aeth-burn-days-peaked/' + stations[i] + '.csv')

        df.to_csv(outPath)

    print('[INFO]  Peaks identified ..')

    # -------------------------------------------------------------------------
    #

    dataFile = os.path.expanduser(
        cwd + '/dynamic/data/aeth-burn-days-peaked/BV.csv')
    dfBV = pd.read_csv(dataFile, index_col=0, header=0)

    dataFile = os.path.expanduser(
        cwd + '/dynamic/data/aeth-burn-days-peaked/LN.csv')
    dfLN = pd.read_csv(dataFile, index_col=0, header=0)

    dataFile = os.path.expanduser(
        cwd + '/dynamic/data/aeth-burn-days-peaked/SM.csv')
    dfSM = pd.read_csv(dataFile, index_col=0, header=0)

    # count the peaks by summing peak occurences for each station
    peak_count_BV = sum(1 for x in dfBV['Peak'] if float(x) == 1)
    peak_count_LN = sum(1 for x in dfLN['Peak'] if float(x) == 1)
    peak_count_SM = sum(1 for x in dfSM['Peak'] if float(x) == 1)

    # ------------------------------------------------------------

    dfBV['DATETIME'] = pd.to_datetime(dfBV['DATETIME'])
    dfLN['DATETIME'] = pd.to_datetime(dfLN['DATETIME'])
    dfSM['DATETIME'] = pd.to_datetime(dfSM['DATETIME'])

    # ------------------------------------------------------------
    # count peak occurences in months, days, and hours, and burn-day-types

    # count peaks occuring per month
    BV_month_counts = countMonth(dfBV)
    LN_month_counts = countMonth(dfLN)
    SM_month_counts = countMonth(dfSM)

    # count peaks occuring per weekday
    BV_day_counts = countDay(dfBV)
    LN_day_counts = countDay(dfLN)
    SM_day_counts = countDay(dfSM)

    # count peaks occuring per hour of day
    BV_hour_counts = countHour(dfBV)
    LN_hour_counts = countHour(dfLN)
    SM_hour_counts = countHour(dfSM)

    # count peaks occuring based on burn type of days
    BV_burn_counts = countBurn(dfBV)
    LN_burn_counts = countBurn(dfLN)
    SM_burn_counts = countBurn(dfSM)

    # ------------------------------------------------------------
    # count peak occurences in each hour of the day, based on day-type

    BV_hourOK_counts, BV_hourMOD_counts, BV_hourBAD_counts = countHourTypes(
        dfBV)
    LN_hourOK_counts, LN_hourMOD_counts, LN_hourBAD_counts = countHourTypes(
        dfLN)
    SM_hourOK_counts, SM_hourMOD_counts, SM_hourBAD_counts = countHourTypes(
        dfSM)

    # ------------------------------------------------------------
    # count peak occurences day of the week, based on day-type

    BV_dayOK_counts, BV_dayMOD_counts, BV_dayBAD_counts = countDayTypes(dfBV)
    LN_dayOK_counts, LN_dayMOD_counts, LN_dayBAD_counts = countDayTypes(dfLN)
    SM_dayOK_counts, SM_dayMOD_counts, SM_dayBAD_counts = countDayTypes(dfSM)

    # ------------------------------------------------------------
    # call on the plotters
    print('[INFO]  Peak plotting begins ..')
    weekDays = ('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN')
    hours = ('00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
             '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00')

    plotterPeaksDays(BV_day_counts, LN_day_counts,
                     SM_day_counts, weekDays, 'DOW')
    plotterPeaksHours(BV_hour_counts, LN_hour_counts,
                      SM_hour_counts, hours, 'HOD')

    title = 'Delta Carbon Events for Monitored Areas by Hour of Day (Unrestricted)\n\n'
    plotterPeaksHoursType(BV_hourOK_counts, LN_hourOK_counts,
                          SM_hourOK_counts, hours, 'HOD_1', title)
    title = 'Delta Carbon Events for Monitored Areas by Hour of Day (Voluntary No-Burn)\n\n'
    plotterPeaksHoursType(BV_hourMOD_counts, LN_hourMOD_counts,
                          SM_hourMOD_counts, hours, 'HOD_2', title)
    title = 'Delta Carbon Events for Monitored Areas by Hour of Day (Mandatory No-Burn)\n\n'
    plotterPeaksHoursType(BV_hourBAD_counts, LN_hourBAD_counts,
                          SM_hourBAD_counts, hours, 'HOD_3', title)

    title = 'Delta Carbon Events for Monitored Areas by Day of Week (Unrestricted)\n\n'
    plotterPeaksHoursType(BV_dayOK_counts, LN_dayOK_counts,
                          SM_dayOK_counts, weekDays, 'DOW_1', title)
    title = 'Delta Carbon Events for Monitored Areas by Day of Week (Voluntary No-Burn)\n\n'
    plotterPeaksHoursType(BV_dayMOD_counts, LN_dayMOD_counts,
                          SM_dayMOD_counts, weekDays, 'DOW_2', title)
    title = 'Delta Carbon Events for Monitored Areas by Day of Week (Mandatory No-Burn)\n\n'
    plotterPeaksHoursType(BV_dayBAD_counts, LN_dayBAD_counts,
                          SM_dayBAD_counts, weekDays, 'DOW_3', title)

    print('[INFO]  Plotting successful ..')
    print()
