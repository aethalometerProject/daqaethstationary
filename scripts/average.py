import pandas as pd
from datetime import datetime, timedelta


def averageDelta(frame, idx):

    outFrame = pd.DataFrame(index=idx, columns=(
        'DATE', 'delta24', 'delta24minus'))
    outFrame = outFrame.fillna(0)

    i = 0
    j = 0

    while i < len(frame):

        # grab date for active day (the i-th row)
        dateTemp = frame['DATE'].iloc[i]

        # grab all rows with active date as slice
        dataTemp = frame[frame['DATE'] == dateTemp]

        # write active date to j-th row of outframe
        outFrame['DATE'].iloc[j] = dateTemp

        # write 24-hr average of slice to j-th row of outframe
        outFrame['delta24'].iloc[j] = (dataTemp['delta'].sum()) / 24

        # not certain we need the delta24minus column

        if i > 0:
            dateTemp = frame['DATE'].iloc[i - 1]
            dataTemp = frame[frame['DATE'] == dateTemp]
            outFrame['delta24minus'].iloc[j] = outFrame['delta24'].iloc[j] - \
                dataTemp['delta'].iloc[23]

        i = i + 24
        j = j + 1

    outFrame['DATE'] = pd.to_datetime(outFrame['DATE'])

    return outFrame


def averagePM(df, idx):

    outFrame = pd.DataFrame(index=idx, columns=('DATE', 'DT_24'))
    outFrame = outFrame.fillna(0)

    i = 0
    j = 0
    while i < len(df):

        dateTemp = df['DATE'].iloc[i]    # date for the 24-hr range

        # grab slice of main frame
        dataTemp = df[df['DATE'] == dateTemp]
        outFrame['DATE'].iloc[j] = dateTemp

        # average this slice of DT
        outFrame['DT_24'].iloc[j] = (dataTemp['DT'].sum()) / 24

        i = i + 24
        j = j + 1

    outFrame['DATE'] = pd.to_datetime(outFrame['DATE'])

    return outFrame


def averageTemp(df, idx):

    outFrame = pd.DataFrame(index=idx, columns=('DATE', 'TEMP_24'))
    outFrame = outFrame.fillna(0)

    i = 0
    j = 0

    while i < len(df):

        dateTemp = df['DATE'].iloc[i]    # date for the 24-hr range
        # grab slice of main frame
        dataTemp = df[df['DATE'] == dateTemp]

        outFrame['DATE'].iloc[j] = dateTemp
        outFrame['TEMP_24'].iloc[j] = (
            dataTemp['TEMP'].sum()) / 24  # average this slice of DT

        i = i + 24
        j = j + 1

    outFrame['DATE'] = pd.to_datetime(outFrame['DATE'])

    return outFrame
