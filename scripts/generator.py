# LIBRARIES: dependencies are loaded
import pandas as pd
# import csv
import os
from datetime import datetime, timedelta

from scripts.average import averageDelta, averagePM, averageTemp
from scripts.plotter import plotter


import matplotlib.pyplot as plt
#import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# For Deleting Files
import glob

pd.options.mode.chained_assignment = None


def generator(startDate, endDate, stations):

    cwd = os.getcwd()
    # print(cwd)

    # ------------------------------------------------------------------------------
    # PLOTTER HELPS: NEEDED FOR LATEX
    path = '/Library/TeX/texbin:'
    path2 = '/Library/TeX/texbin/kpsewhich:'
    path3 = '/Library/TeX/texbin/latex:'
    os.environ['PATH'] += path
    os.environ['PATH'] += path2
    os.environ['PATH'] += path3

    # ------------------------------------------------------------------------------
    # startDate = 20181130  # NOTE: range is exclusive of startDate

    # ------------------------------------------------------------------------------
    # VARIABLES: static args declaredimport pandas as pd
    # dates = []
    startDate = str(startDate)
    startDTO = datetime.strptime(startDate, '%Y%m%d')
    startDTOadj = startDTO + timedelta(1)
    endDate = str(endDate)
    endDTO = datetime.strptime(endDate, '%Y%m%d')

    timeD = endDTO - startDTO
    days = int(timeD.days)
    print('[INFO]  ' + str(days) +
          ' days of data will be loaded into analysis.csv')

    # sets the initial date for the loop
    dateDTO = endDTO

    # sets the path for the temporary csv data; clears csv contents

    dataPath = os.path.expanduser(cwd + '/dynamic/holder.csv')
    f = open(dataPath, 'w+')
    f.close()

    #stations = ['bv', 'rp', 'ln', 'sm']

    # ------------------------------------------------------------------------------

    for j in range(len(stations)):
        # print(stations[j])

        for i in range(days):

            # create a path for the dated file we will fetch
            dateExtension = dateDTO.strftime('%Y-%m-%d') + str('.csv')
            pathBase = str(cwd + '/static/data/') + \
                stations[j] + '/' + stations[j] + \
                '-1hr-ae33-7-' + dateExtension
            # print(pathBase)
            datePath = os.path.expanduser(pathBase)

            # fetch the targeted csv file and read into a dataframe
            try:
                dateFrame = pd.read_csv(datePath, index_col=0, header=None)
            except FileNotFoundError:
                print("[INFO]  No Data for " +
                      stations[j] + 'for date ' + dateExtension)

            # append this data into the analysis.csv file
            with open(dataPath, 'a') as f:
                (dateFrame).to_csv(f, header=False)
            f.close()

            # adjust date to one day prior for next loop iter
            dateDTO = dateDTO - timedelta(1)

        dateDTO = endDTO

    # ------------------------------------------------------------------------------
    # load the csv-stored data into a pandas frame called data

    data = pd.read_csv(dataPath, index_col=0, header=None)

    # trim unnecessary columns off; insert header with named values
    data = data.iloc[:, 0:11]
    cols = ['UNIT', 'DATE', 'DATETIME', 'CH1', 'CH2',
            'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'FLOW']
    data.columns = cols

    # convert column 3 of the data df to a datetime type; can check the datatypes of each col
    data['DATETIME'] = pd.to_datetime(data['DATETIME'])

    # ------------------------------------------------------------------------------
    # DATA CONSTRUCTOR: Delta Carbon & Temperature Deficit
    # find brown carbon count .. this is delta of columns 8,3
    data['delta'] = (data['CH1'] - data['CH6']) * 1000

    # ------------------------------------------------------------------------------
    # build three unique sets (split across 0507, 0508, 0509 .. column 1)
    # these sets contain the brown carbon data for each of three monitoring stations

    delta_dict = {'bv': 508, 'ln': 507, 'sm': 509}

    delta_list = []
    for station in stations:

        delta_loc = data[data['UNIT'] == delta_dict[station]]
        delta_loc = delta_loc.sort_values('DATETIME', ascending=True)
        delta_list.append(delta_loc)

    # ------------------------------------------------------------------------------
    # find average brown carbon over 24 hr periods beginning at midnight

    avg_delta_list = []
    idx = pd.date_range(startDTO + timedelta(hours=24), periods=days, freq='D')
    for delta in delta_list:
        avg_delta_list.append(averageDelta(delta, idx))

    # ------------------------------------------------------------------------------
    # DAY TYPE INFO

    # Load Burn Day Type Data
    DayType_Path = os.path.expanduser(cwd + '/static/data/burn-days.csv')
    DayTypeDF = pd.read_csv(DayType_Path)

    DayTypeDF.Date = DayTypeDF.Date.astype(str)

    DayTypeDF.Date = pd.to_datetime(DayTypeDF.Date)

    DayTypeDF = DayTypeDF.replace([3, 1, 2], ['r', 'g', 'orange'])

    DayTypeDF.set_index('Date', inplace=True)

    endDTOadj = endDTO + timedelta(1)

    Selected_Days = DayTypeDF[startDTOadj:endDTOadj]

    # Selected days for each county
    BV_Selected_Days = Selected_Days['Davis']
    LN_Selected_Days = Selected_Days['Utah']
    SM_Selected_Days = Selected_Days['Cache']
    RP_Selected_Days = Selected_Days['Salt Lake']

    print('[INFO]  Successfully loaded air restriction data (day-types)')

    # -----------------------------------------------------------------------------
    # load corrected heat deficit data
    # this is static HD data for the period of the study stored in a CSV file

    dataHDFixed24Path = os.path.expanduser(
        cwd + '/static/data/HeatDef-corrected24hr.csv')
    dataHDFixed24 = pd.read_csv(dataHDFixed24Path, header=0)
    cols = ['DATE', 'HD24']
    dataHDFixed24.columns = cols
    dataHDFixed24['DATE'] = pd.to_datetime(dataHDFixed24['DATE'])

    # -----------------------------------------------------------------------------
    # TRIM HD-CORRECTED FOR USE IN PLOTTERS

    startDTOtemp = startDTO + timedelta(hours=24) + timedelta(hours=12)
    startIdxVal = dataHDFixed24[dataHDFixed24.DATE ==
                                startDTOtemp].first_valid_index()
    endDTOtemp = endDTO + timedelta(hours=12)
    endIdxVal = dataHDFixed24[dataHDFixed24.DATE ==
                              endDTOtemp].first_valid_index()

    dataHDFixed24Trim = dataHDFixed24[startIdxVal:endIdxVal + 1]

    print('[INFO]  Successfully loaded 24-hr averaged heat-deficit data')

    #------------------------------------------------------------------------------
    # DATA LOADER: DT and TEMP ()

    dttPathBV = os.path.expanduser(
        cwd + '/static/data/temperature-pm/BV_PM2.5Temp.csv')
    dttPathLN = os.path.expanduser(
        cwd + '/static/data/temperature-pm/LN_PM2.5Temp.csv')
    dttPathSM = os.path.expanduser(
        cwd + '/static/data/temperature-pm/SM_PM2.5Temp.csv')

    dttBV = pd.read_csv(dttPathBV)
    dttLN = pd.read_csv(dttPathLN)
    dttSM = pd.read_csv(dttPathSM)

    # data: set date column to type string
    # data: set date column to timestamp object; to_datetime() is deprecated
    dttBV['DATE'] = dttBV['DATE'].astype(str)
    dttLN['DATE'] = dttLN['DATE'].astype(str)
    dttSM['DATE'] = dttSM['DATE'].astype(str)
    dttBV['DATE'] = pd.to_datetime(dttBV['DATE'])
    dttLN['DATE'] = pd.to_datetime(dttLN['DATE'])
    dttSM['DATE'] = pd.to_datetime(dttSM['DATE'])

    # get the index of first instance of the needed datetime object

    startDTOtemp = startDTO + timedelta(hours=24)
    startIdxVal = dttBV[dttBV.DATE == startDTOtemp].first_valid_index()
    endIdxVal = dttBV[dttBV.DATE == endDTO].first_valid_index()
    endIdxVal = endIdxVal + 23

    dttBV = dttBV.loc[startIdxVal:endIdxVal]
    dttLN = dttLN.loc[startIdxVal:endIdxVal]
    dttSM = dttSM.loc[startIdxVal:endIdxVal]

    dtt_list = [dttBV, dttLN, dttSM]

    print('[INFO]  Successfully loaded temperature and PM2.5 data')
    print()

    # -----------------------------------------------------------------------------
    # PLOTTER CALLS FOR AETH vs HEAT-DEFICIT
    print('[INFO]  AE33 vs Heat-Deficit Plotting begins ..')

    file_names = []
    dayTypeList = [BV_Selected_Days, LN_Selected_Days, SM_Selected_Days]

    plot_dir = '/dynamic/plots/'

    for j in range(len(stations)):

        location = stations[j]

        upper_location = location.upper()

        root = 'plot_' + upper_location + '_AE24-HD24' + '.png'
        file_name = plot_dir + root

        file_names.append(root)

        plotter(avg_delta_list[j], dataHDFixed24Trim, dayTypeList[j], 'delta24', 'HD24', upper_location,
                file_name, 'C4', 'o--', "Delta-C", "Heat Deficit", "[MJ/m$^2$]", startDTO, endDTO)

    # -------------------------------------------------------------
    # find average for PM2.5 data

    avg_PM_list = []

    idx = pd.date_range(startDTO + timedelta(hours=24), periods=days, freq='D')

    for dtt in dtt_list:
        avg_PM_list.append(averagePM(dtt, idx))

    # -------------------------------------------------------------
    # plotter calls for AE33 vs PM2.5
    print('[INFO]  AE33 vs DTT Plotting begins ..')

    file_names = []

    # static_dir = '/static/'
    plot_dir = '/dynamic/plots/'

    for j in range(len(stations)):

        location = stations[j]

        upper_location = location.upper()

        root = 'plot_' + upper_location + '_AE24-PM24' + '.png'
        file_name = plot_dir + root

        file_names.append(root)

        plotter(avg_delta_list[j], avg_PM_list[j], dayTypeList[j], 'delta24', 'DT_24', upper_location,
                file_name, 'C3', '--', "Delta-C", "PM 2.5", "[mg/m$^3$]", startDTO, endDTO)

    # -------------------------------------------------------------
    # find average for temperature data

    avg_TEMP_list = []

    idx = pd.date_range(startDTO + timedelta(hours=24), periods=days, freq='D')
    for dtt in dtt_list:
        avg_TEMP_list.append(averageTemp(dtt, idx))

    # -------------------------------------------------------------
    # plotter calls for AE33 vs Temperature
    print('[INFO]  AE33 vs Temperature Plotting begins ..')

    file_names = []

    #static_dir = '/static/'
    plot_dir = '/dynamic/plots/'

    for j in range(len(stations)):

        location = stations[j]

        upper_location = location.upper()

        root = 'plot_' + upper_location + '_AE24-TEMP24' + '.png'
        file_name = plot_dir + root

        file_names.append(root)

        plotter(avg_delta_list[j], avg_TEMP_list[j], dayTypeList[j], 'delta24', 'TEMP_24', upper_location,
                file_name, 'C5', '--', "Delta-C", "Temperature", "[C]", startDTO, endDTO)

    print('[INFO]  Plotting successful ..')
    print()

# #------------------------------------------------------------------------------
