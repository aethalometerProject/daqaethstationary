from scripts.generator import generator
from scripts.generatorPeaks import generatorPeaks
import os
import glob

# clear the system terminal window
os.system('cls' if os.name == 'nt' else 'clear')

cwd = os.getcwd()
print(cwd)


# instruction printer section
print('   Welcome to the University of Utah - DAQ Aethalometer Delta-Carbon Analyzer  ')
print('-------------------------------------------------------------------------------')
print()
print('INSTRUCTIONS:    * Please enter START and END DATES for the desired range.')
print('                 * Enter these dates using the format: YYYYMMDD')
print('                 * Valid date range for study: 20181130 through 20190228')
print('                   Script will error if dates given are outside this window.')
print()
print('                 * Enter the desired STATIONS as some combination of bv ln sm')
print('                 * These must be entered as comma separated without spaces')
print('                 * These must be entered in lower-case')
print('                 * e.g.,  bv,ln,sm  OR  bv,sm  OR  ln,sm,bv')
print()
print('                 * NOTE: Rerunning this script deletes any previous plot output')
print('                         Save any needed files before rerunning!')
print()
print('                 * NOTE: peak identification plots produced for entire study.')
print('                         i.e., these plots will be produced for all stations')
print('                               over the full study range (20181201-20190228).')
print()
print('-------------------------------------------------------------------------------')

# get user date range
startDate = input('Start Date: ')
startDate = int(startDate)
endDate = input('  End Date: ')
endDate = int(endDate)
print()
stations = input("Station(s): ")
print()
print('-------------------------------------------------------------------------------')
print('[INFO]  Analysis begins ..')
print()
stations = stations.split(",")

# clear the plots directory
files = glob.glob(cwd + '/dynamic/plots/*')
for f in files:
    os.remove(f)

# function call for generating requested plots (AE33 vs [HD,PM2.5,TEMP])
generator(startDate, endDate, stations)

# function call for identifying peaks and giving bar plot results
generatorPeaks()

# final print lines
print('[INFO]  All plotting completed successfully ..')
print('[INFO]  Plot(s) have been saved locally at:')
print('        ' + cwd + '/dynamic/plots/')
print()
