README file for daqAethStationary Repository

This respository contains code producing results from the stationary
portion of the DAQ-University of Utah Aethalometer Study (2019).

Via terminal run the main.py script to produce plots.

**Prior to running main.py please run init.py to unzip required data.

INPUT PARAMETERS:
Start Date
End Date
Station ID(s)

OUTPUTS
Delta-Carbon Peak Events (8 plots)
Delta-Carbon v Heat Deficit (1 plot per station)
Delta-Carbon v Particulate Content (1 plot per station)
Delta-Carbon v Temperature (1 plot per station)

DATA FILE DESCRIPTIONS
Data files are located in the static/data directory.
Four types of data files are included:
type of day, aethalometer, temperature-pm, heat-deficit

/static/data/aeth-burn-days
these csv files contain all aethalometer data

however only the day type column from these are used

/static/data/{bv,ln,rp,sm}
these directories contain aethalometer data for each station
Columns are: (comm channel, unit #, date, datetime, CH1, CH2, CH3, CH4, CH5, CH6, CH7, flow)
flow units are (L/min)
wavelength measurements (by channel) are given as
CH1 370 nm
CH2 470 nm
CH3 520 nm
CH4 590 nm
CH5 660 nm
CH6 880 nm
CH7 950 nm
delta-carbon is calculated using (CH1 - CH6)*1000; this gives tentative units of (microgram/m^3)
excess columns are present, but not used (all are zero valued)

/static/data/temperature-pm
this directory contains PM2.5 and temperature readings for BV, LN, SM stations
pm2.5 units are (mg/m^3); temperature units are (degrees C)
columns are: (datetime, pm2.5, temperature)

/static/data/burn-days.csv
this file contains day-type information
1 implies no restriction
2 implies voluntary restriction
3 implies non-vonlutary restriction

/static/data/HeatDef-corrected24hr.csv
heat deficit units are (MJ/m^2)
two columns: datetime, heatdeficit
