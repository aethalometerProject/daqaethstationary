import zipfile
import os

# extract data files to the static directory
cwd = os.getcwd()

zip_ref = zipfile.ZipFile(cwd + '/static/data.zip', 'r')
zip_ref.extractall(cwd + '/static/')
zip_ref.close()


